# Project Builder

## Get Last Version

``docker-compose pull``

## Run

``docker-compose.exe up -d --remove-orphans -V --force-recreate``

## Stop

``docker-compose.exe down``
